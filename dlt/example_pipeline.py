import pandas as pd

import dlt
from dlt.sources.helpers import requests

pipelines = [
    {
        "pipeline_name": "main_pipeline",
        "destination": "duckdb",
        "dataset_name": "testing",
        "table_name": "player",
        "write_disposition": "replace",
    },
    {
        "pipeline_name": "main_pipeline",
        "destination": "duckdb",
        "dataset_name": "testing",
        "table_name": "natural_disasters",
        "write_disposition": "replace",
    },
]


# Grab some player data from Chess.com API
data = []

for player in ["magnuscarlsen", "rpragchess"]:
    response = requests.get(f"https://api.chess.com/pub/player/{player}")
    response.raise_for_status()
    data.append(response.json())
# Extract, normalize, and load the data

owid_disasters_csv = (
    "https://raw.githubusercontent.com/owid/owid-datasets/master/datasets/"
    "Natural%20disasters%20from%201900%20to%202019%20-%20EMDAT%20(2020)/"
    "Natural%20disasters%20from%201900%20to%202019%20-%20EMDAT%20(2020).csv"
)
df = pd.read_csv(owid_disasters_csv)
data = df.to_dict(orient="records")



for pipeline in pipelines:
    pipe = dlt.pipeline(
        pipeline_name=pipeline["pipeline_name"],
        destination=pipeline["destination"],
        dataset_name=pipeline["dataset_name"],
    )

    load_info = pipe.run(
        data=data,
        table_name=pipeline["table_name"],
        write_disposition=pipeline["write_disposition"],
    )

    print(f"{150*'*'}")
    print(f"Pipeline: {pipeline['pipeline_name']}")
    print(load_info)

# duckdb main_pipeline.duckdb
# use main_pipeline;
# use testing;
# show tables;
# SELECT * FROM natural_disasters;
# SELECT * FROM player;