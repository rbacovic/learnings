# Resources

* Agenda: [GitLab - GCP Curriculum - Fall 2023-2024](https://docs.google.com/spreadsheets/d/1r7UqY2aZVHsxich0sK_O--ush6NhR1aX7sZzXzxxrJg/edit?resourcekey=0-53Iil-d_dvp8vqutYfUAuw#gid=1849771676)
* [Labs](https://googlecloud.qwiklabs.com/)
* [All labs](https://drive.google.com/drive/folders/11ylWSCRVcFS3RoWj0fKLNB-82HLs1MFu)
* [All lectures](https://drive.google.com/drive/folders/11gqg2RPBr-wB4jYFcPTlDZW9Hr4nWAUI)


## Materials
* [SRE Google](https://sre.google/) - go to Latest resources
* [SRE Google course slides - The Art of SLOs](https://static.googleusercontent.com/media/sre.google/en//static/pdf/art-of-slos-slides.pdf)
* [quic - basic info](https://peering.google.com/#/learn-more/quic)
* [Cloud locations](https://cloud.google.com/about/locations#network)
* [Hybrid NEG concepts](https://cloud.google.com/load-balancing/docs/negs/hybrid-neg-concepts)
* [GCP deletion process](https://services.google.com/fh/files/misc/gcp_data_deletion_nda.pdf)
* [Google Cloud KMS deep dive](https://cloud.google.com/docs/security/key-management-deep-dive/resources/google-cloud-kms-deep-dive.pdf)
* [Google Infrastructure Whitepaper](https://cloud.google.com/static/docs/security/infrastructure/design/resources/google_infrastructure_whitepaper_fa.pdf)
* [Book: Builfing Secure and Reliable Systems](https://theswissbay.ch/pdf/Books/Computer%20science/building_secure_and_reliable_systems.pdf)
* [Create Public Uptime Checks with Cloud Monitoring](https://www.youtube.com/watch?v=rpE5mLfgqmE)
* [Private uptime check]( https://cloud.google.com/monitoring/uptime-checks/private-checks)
* [Stack doctor](https://cloud.google.com/blog/products/operations/verify-gke-services-are-up-with-dedicated-uptime-checks)



# Day 1

* Kubernetes basics
* Elements
    * pods
    * deployment
    * NodePool
    * namespaces (default: `Kube-system`, `Kube-public`, `default`)

* Commands 
![Kubect_commands](kubectl_commands.png)

* Deployments, Jobs, Scaling
    * When creting service, should access using its IP (not using pods IP)
    * Various strategies for deployment (Recreate, Rolling update, Blue/green, A/B, Shadow
    * [Istio Mesh](https://istio.io)
    * Rollback strategy? What is the approach?

# Day 2

* Network generally
* Security
* Kuberenetes DNS (`kube-dns`)
* Data persistance
* Persistent volume / Claim Persistent Volume (in `gcp`, it is not `Volume` but `Disk`)
* ConfigMaps and Secrets

# Day 3

* IAM in details
* Roles
    * base roles
    * pre-defined roles
    * custom roles
* Credentials or IP rotation - high level operation, fully manager gby GCP
* Logging
![logging](logging.png)
* [Storage advisor](https://cloud.google.com/static/architecture/images/storage-advisor.svg)
* [Autoclass](https://cloud.google.com/storage/docs/autoclass)
* Databases
    * [Cloud `Spanner` trims entry cost by 90%, offers sharper observability and easier querying](https://cloud.google.com/blog/products/databases/get-more-out-of-spanner-with-granular-instance-sizing)
    * `Firestore`
    * `Alloy DB` similar to AWS Aurora
    * [`BigQuery` Omni](https://cloud.google.com/bigquery/docs/omni-introduction#query-data)
* [Secret manager](https://cloud.google.com/security/products/secret-manager)
* [Workload identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity)
* [Using Common Expression Language](https://cloud.google.com/certificate-authority-service/docs/using-cel)
    * [Deny policy overview](https://cloud.google.com/iam/docs/deny-overview)
    * [CEL specification](https://github.com/google/cel-spec)

# Q&A

* [Delete a namespace 1/2](https://kubernetes.io/docs/tasks/administer-cluster/namespaces/#deleting-a-namespace)
* [Kuberenetes: delete a namespace 2/2](https://phoenixnap.com/kb/kubernetes-delete-namespace)
* What is the best way to monitor the PVC volume in GKE ? - the way to approach is in the [link](https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/preexisting-pd)
    * [GKE how to use existing compute engine disk as persistent volumes](https://stackoverflow.com/questions/66598748/gke-how-to-use-existing-compute-engine-disk-as-persistent-volumes)
