import os
from datetime import datetime, timedelta
import time
import random

import polars as pl
from faker import Faker

# optional, in case your files are not in the working directory
# os.chdir("/path/")
file_path = ""

Faker.seed(0)  # optional, seed value if you want to return consistent data
fake = Faker("en_CA")  # change this to your locale
random.seed(10)  # optional, seed value if you want to return consistent data

# added to measure elapsed time
start = time.perf_counter()

# Set number of rows and chunks for the Polars to create
n_rows = 200_000
n_chunks = 15
chunk_size = int(n_rows / n_chunks)

# Set date range for the dataset
start_date = datetime(2022, 1, 1)
end_date = datetime(2022, 12, 31)


def create_fake_data():
    '''
    Generates a fake dataset with 5 columns.
    Number of rows can be defined. Data generated in chunks using
    Python Polars library (faster for initial smaller number of rows).
    Returns a dataframe materialized as a parquet file.
    '''
    # Generate random dates between the date range
    days = (end_date - start_date).days
    dates = [
        start_date + timedelta(days=random.randint(0, days))
        for i in range(chunk_size)
    ]
    random.shuffle(dates)

    # Create a list of 10 random alpha-numeric values for the 'Item' column
    items = [
        "".join(
            random.choices(
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
                k=10,
            )
        )
        for i in range(100)
    ]

    # Create a list of 20 fake company names
    company = [fake.company() for i in range(20)]

    # Create a DataFrame with five columns
    # loop through each chunk and append it to the Parquet file

    df = None
    df_chunk = None

    for i in range(1, n_chunks + 1):

        # Generate random dates between the date range
        days = (end_date - start_date).days
        chunk_dates = [
            start_date + timedelta(days=random.randint(0, days))
            for i in range(chunk_size)
        ]
        random.shuffle(chunk_dates)

        # Create a DataFrame with five columns
        df_chunk = pl.DataFrame(
            {
                "Date": [d.date() for d in chunk_dates],
                "Company": [random.choice(company) for i in range(chunk_size)],
                "Item": [random.choice(items) for i in range(chunk_size)],
                "On_hand": [random.randint(0, 100) for i in range(chunk_size)],
                "sales": [random.randint(0, 1000) for i in range(chunk_size)],
            }
        ).with_columns(pl.col("Date").cast(pl.Date))  #

        if i == 1:
            df = df_chunk
        else:
            df = df.collect().vstack(df_chunk)

        df = df.lazy()

        if df is not None:
            df.sink_parquet(
                file_path + "chunk.parquet",
                row_group_size=chunk_size,
                slice_pushdown=True,
                compression="zstd",
            )


create_fake_data()

stop = time.perf_counter()
elapsed = stop - start
print(f"creating dataframe with {n_rows} rows took {elapsed}")

# See next section which takes the 20M row df created by Polars
# and append that 5 times using
# Spark to create a 100M dataframe materialized as a parquet file
# Function “create_fake_data” takes around 111 seconds to return 20 million rows (approximately 165 MB). Next section is the pyspark module.