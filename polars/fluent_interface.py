import polars as pl

# Create a DataFrame
df = pl.DataFrame(
    {
        "Fruit": ["Apple", "Banana", "Cherry", "Date",
                  "Elderberry", "Fig", "Guava"],
        "Quantity": [15, 25, 5, 12, 14, 8, 10],
        "Price": [1.5, 0.5, 2.0, 1.0, 2.5, 1.75, 1.0]
    }
)


# def filter_by_quantity(df):
#     return df.filter(pl.col("Quantity") > 10)
#
#
# def sort_by_price(df):
#     return df.sort("Price")
#
#
# def group_and_agg(df):
#     return df.groupby("Fruit").agg([pl.mean("Price")])
#
#
# df = (df
#       .pipe(filter_by_quantity)
#       .pipe(sort_by_price)
#       .pipe(group_and_agg)
#       )


df = (df.filter(pl.col("Quantity") > 10)
      .groupby("Fruit")
      .agg([pl.avg("Price").alias("AVG_PRICE"), pl.sum("Quantity").alias("SUM_QUANTITY"), (pl.sum(pl.col("Quantity")*pl.col("Quantity"))).alias("TOTAL")])
      .sort("TOTAL", descending=True)
      )


print(df.head())
