import random
from datetime import datetime
import polars as pl
import time

customers = []
LETTERS = 'abcdefghijklmnopqrstuvwxyz'
REC_NO = 1000000


def create_fake_data(nr_of_rows, path, file):
    '''
    Create a fake transactions dataset and materialize to defined format.
    Dataframe can be made as large as required using required argument 'nr_of_row'
    Other required arguments are 'path' and 'filename'
    '''

    for customers_id in range(nr_of_rows):
        # Create transaction date in range
        d1 = datetime.strptime('1/1/2021', '%m/%d/%Y')
        d2 = datetime.strptime('12/31/2021', '%m/%d/%Y')

        # get the timestamp of the beginning and end of the range
        start_timestamp = int(d1.timestamp())
        end_timestamp = int(d2.timestamp())

        # generate a random timestamp within the range.
        random_timestamp = random.uniform(start_timestamp, end_timestamp)
        transaction_date = datetime.fromtimestamp(random_timestamp)
        transaction_date = transaction_date.strftime('%m/%d/%Y %H:%M:%S.%f')

        # create customer IDs
        cust_id = random.randint(1, 135)

        # Create shipping method
        ship_method = random.choice(['Truck', 'Air', 'Rail'])

        # Create Item #s
        item = random.randint(1, 1000)

        # create reference numbers
        random_letters = ''.join(random.sample(LETTERS, k=3))
        random_numbers = ''.join(str(random.randint(0, 9)) for _ in range(7))
        ref = f'id-{random_numbers}{random_letters}' + ';' + \
              f'id-{random_numbers[::-1]}{random_letters}'

        # create list with the generated elements
        customers.append([transaction_date, ref, cust_id, ship_method, item])

    # covert list to dataframe using Polars
    df = pl.DataFrame(customers)

    df.columns =['Transaction_date', 'Ref','Cust_ID', 'Ship_method', 'Item']

    # print(df.head())  # debug

    # write file to desired format, Parquet used in this example.
    # The arguments 'use-arrow' and 'compression' are optional,
    # but we found that specifying values resulted in faster write speeds
    df.write_parquet(f'{file}.parquet',
                     use_pyarrow=True, compression='zstd')


start_time = time.time()  # optional: record start time

create_fake_data(REC_NO, '', 'output_file_name')

# optional: record execution time
end_time = time.time()
execution_time = end_time - start_time
print(f"Execution time: {execution_time} seconds")
