import polars as pl

FILE_NAME = "output_file_name.parquet"

# scan data, lazy loading
df = pl.scan_parquet(FILE_NAME)

print(df)

# read data
df = pl.read_parquet(FILE_NAME)

# contact DF vertically
df_vertical_contact = pl.concat([df, df])

print(df_vertical_contact.shape)

# Drop duplicates
df_back = df_vertical_contact.unique()

print(df_back.shape)
