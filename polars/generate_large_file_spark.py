import sys
from datetime import datetime, timedelta
import time
import os

from pyspark.sql import SparkSession

# os.chdir("/path/")
file_path = ""

start = time.perf_counter()

def spark_union():
    # create a SparkSession
    spark = SparkSession.builder.appName("Append Parquet Files").getOrCreate()

    df1 = spark.read.parquet(file_path + "chunk.parquet")
    df2 = spark.read.parquet(file_path + "chunk.parquet")
    df3 = spark.read.parquet(file_path + "chunk.parquet")
    df4 = spark.read.parquet(file_path + "chunk.parquet")
    df5 = spark.read.parquet(file_path + "chunk.parquet")

    # union the two dataframes
    df_combined = df1.union(df2).union(df3).union(df4).union(df5)

    # write out the combined dataframe to a new Parquet file
    # replace 'overwrite' with 'append', even faster but creates multiple files
    df_combined.write.mode("overwrite").parquet(
        file_path + "large.parquet"
    )

    # read in the Parquet file lazily
    df = spark.read.parquet(file_path + "large.parquet")

    # view the contents of the DataFrame
    df.show()
    df.printSchema()

    # get the row count of the DataFrame
    row_count = df.count()

    # print the row count

    return row_count

    # stop the SparkSession
    spark.stop()

if __name__ == '__main__':
    print(spark_union())