import polars as pl

FILE_NAME = "output_file_name.parquet"
# read data

df = pl.scan_parquet(FILE_NAME)
print(df.schema)

df = df.rename({"Transaction_date": "date", "Ref": "reference"})
print(df.schema)

df = df.with_columns([
    pl.col("Cust_ID").cast(pl.Utf8)
])
print(df.schema)

''' Right way to convert string to date'''
df = (
    df.with_columns([
        pl.col(["date"]).str.strptime(
            pl.Date,
            format='%m/%d/%Y %H:%M:%s.%6f', strict=False
        )
    ])
)

print(df.schema)

# df = df.with_columns(pl.col(["reference"]).str.replace(r"id-", ""))

df = df.with_columns(pl.col(["reference"]).str.replace_all(r"id-", ""))

# Split horizontally
# df = df.with_columns(
#     [pl.col("reference").str.split_exact(";",1).struct.rename_fields(["first_part","second_part"]).alias("fields")]).unnest("fields").drop("reference")


# Split vertically
df = (df
      .with_columns(
          pl.col('reference').str.split(by=";")
      )
      .explode(pl.col('reference'))
      )

print(df.fetch(4))


