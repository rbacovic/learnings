import polars as pl
import pandas as pd

# Create a DataFrame
data = {
    'A': [1, 2, 3, 4, 5],
    'B': [6, 7, 8, 9, 10]
}

df = pd.DataFrame(data)
subset_pandas = df.loc[[0, 2, 4], 'A']

print(subset_pandas)

df = pl.DataFrame(data)

subset_polars = df.select(pl.col(['A'])).with_row_count(name='index').filter(pl.col("index").is_in([0, 2, 4]))

print(subset_polars)
