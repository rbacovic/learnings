# learnings



This repo contains learning things to explore new technologies, frameworks and libraries.


# Polars 

## Resources

1. [polars](https://www.pola.rs/) - Lightning-fast DataFrame library for Rust and Python
   1. [**Part 1.** Phenomenal Python Polars: The new go-to for ETL and data analysis](https://medium.com/@danny.bharat/phenomenal-python-polars-the-new-go-to-for-etl-and-data-analysis-6766a92f3bbf)
   2. [**Part 2:** Efficient Data Manipulation with Python Polars: Lazy Frames, Table Combining and Deduplication](https://medium.com/@danny.bharat/python-polars-part2-fe7a0ca4435c)
   3. [**Part 3.** Python Polars - Data Type Operations, Replacements, Extractions, and Explosions](https://medium.com/@danny.bharat/python-polars-transform-recipes-date-extract-explode-4aafa58bd046)
   4. [**Part 4.** Handling Large Datasets with Polars and Spark: Creating a 100 million Row Fake Dataset in Just 2 Minutes](https://medium.com/@danny.bharat/polars-and-spark-100-million-row-dataset-18fd29f46d2c)
   5. [**Part 5.** — Python Polars: Leveraging Fluent Interfaces for Efficient and Reusable Code](https://medium.com/@danny.bharat/part-5-pythons-polars-streamlining-data-processing-fluent-interfaces-in-action-ab8cd31e83d2)
   6. [**Part 6.** — From Pandas to Polars: Breaking Free from Index Dependency](https://medium.com/@danny.bharat/part-6-breaking-free-from-index-dependency-from-pandas-to-polars-1532943eed18)

## Installation

```bash
cd polars/
pip install -r requirements.txt
```

# Advanced Python: 

## Working With Data

* Source [Advanced Python: Working With Data](https://www.linkedin.com/learning/advanced-python-working-with-data?u=2255073)

* [Git repo](https://github.com/LinkedInLearning/advanced-python-working-with-data-4312001)

# Advanced Python: Language Features

* Source [Advanced Python: Language Features](https://www.linkedin.com/learning/advanced-python-language-features/introduction?u=2255073)


# Terraform 3rd Edition

* Source [Terraform: Up and Running, 3rd Edition](https://learning.oreilly.com/library/view/-/9781098116736/)


# Ruff

* Source material: [Ruff](https://docs.astral.sh/ruff/tutorial/#configuration)

# uv package manager

* Source material: [uv](https://github.com/astral-sh/uv)
* [Benchmark test](https://astral.sh/blog/uv)

![uv_benchmark.png](images%2Fuv_benchmark.png)

# dlt

* Source material: [dlt](https://dlthub.com/docs/intro)

# Snowflake 1 Day Performance 

* Date: `2024-06-21`

## Resources 

* [Warehouse sizing](https://medium.com/snowflake/compute-primitives-in-snowflake-and-best-practices-to-right-size-them-b3add53933a3)
* [Deep dive into the internals of Snowflake Virtual Warehouses](https://medium.com/snowflake/deep-dive-into-the-internals-of-snowflake-virtual-warehouses-d6d9676127d2)
* [Events](https://www.snowflake.com/about/events/)
* [Quickstarts](http://quickstarts.snowflake.com)
* [Certification](https://www.snowflake.com/certifications/)
* [User Groups](https://usergroups.snowflake.com/)
* [Summit Keynotes](https://reg.summit.snowflake.com/flow/snowflake/summit24/digitalreg/page/main/?utm_cta=website-summit-save-the-date)
* [Snowflake Account active for 30 days from start of class](https://sfedu03-frb66514.snowflakecomputing.com)
