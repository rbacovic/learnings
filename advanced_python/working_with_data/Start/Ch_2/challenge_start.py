# Example file for Advanced Python: Working With Data by Joe Marini
# Programming challenge: use advanced data collections on the earthquake data

import json
from collections import Counter

# open the data file and load the JSON
with open("../../30DayQuakes.json", "r") as datafile:
    data = json.load(datafile)

quake_list = data["features"]

quake_type_list = []

for q in quake_list:
    quake_type_list.append(q["properties"]["type"])

for k, v in Counter(quake_type_list).items():
    print(f"{k:15} : {v}")
