# Example file for Advanced Python: Working With Data by Joe Marini
# Programming challenge: summarize the earthquake data

import json


# for this challenge, we're going to summarize the earthquake data as follows:
# 1: How many quakes are there in total?
# 2: How many quakes were felt by at least 100 people?
# 3: Print the name of the place whose quake was felt by the most people, with the # of reports
# 4: Print the top 10 most significant events, with the significance value of each

# open the data file and load the JSON
with open("../../30DayQuakes.json", "r") as datafile:
    data = json.load(datafile)


q_totals = len(data["features"])

def is_felt(q):

    if q["properties"]["felt"] is not None and  q["properties"]["felt"] >= 100:
        return True
    return False

print(f"1: How many quakes are there in total: {q_totals}")

q_felt = len(list(filter(is_felt, data["features"])))

print(f"2: How many quakes were felt by at least 100 people: {q_felt}")


def getmostpeople(dataitem):
    most = dataitem["properties"]["felt"]
    if most is None:
        most = 0
    return float(most)

data["features"].sort(key=getmostpeople, reverse=True)

print(f"3: Print the name of the place whose quake was felt by the most people, with the {data['features'][1]['properties']['place']}")


def getsig(dataitem):
    sig = dataitem["properties"]["sig"]
    if sig is None:
        sig = 0
    return float(sig)

data["features"].sort(key=getsig, reverse=True)

print("top 10 most significant events, with the significance value of each")
for s in data["features"][0:10]:
    print(f"{s['properties']['place']} sig: {s['properties']['sig']}")